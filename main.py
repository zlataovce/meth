# bot.py
import os
import keep_alive

import discord
from dotenv import load_dotenv
from random import randint
from fractions import Fraction
from math import gcd

keep_alive.keep_alive()
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

client = discord.Client()


@client.event
async def on_ready():
    for guild in client.guilds:
        print(f'{client.user} is connected to the following guild:\n'
              f'{guild.name}(id: {guild.id})')
        members = '\n - '.join([member.name for member in guild.members])
        print(f'Guild Members:\n - {members}')

    activity = discord.Activity(type=discord.ActivityType.watching,
                                name="a paper")
    await client.change_presence(activity=activity)


def pomer(pocet, pomer1, pomer2):
    if "/" in pomer1:
        pomer_list = pomer1.split("/")
        pomer1 = float(pomer_list[0]) / float(pomer_list[1])
    if "/" in pomer2:
        pomer_list = pomer2.split("/")
        pomer2 = float(pomer_list[0]) / float(pomer_list[1])
    if "/" in pocet:
        pocet_list = pocet.split("/")
        pocet = float(pocet_list[0]) / float(pocet_list[1])
    jeden_dil = float(pocet) / (float(pomer1) + float(pomer2))
    return [jeden_dil * float(pomer1), jeden_dil * float(pomer2)]


def umera(pomer1, pomer2):
    pomer11 = pomer1.split(":")
    if "/" in pomer11[0]:
        pomer_list = pomer11[0].split("/")
        pomer11[0] = float(pomer_list[0]) / float(pomer_list[1])
    if "/" in pomer11[1]:
        pomer_list = pomer11[1].split("/")
        pomer11[1] = float(pomer_list[0]) / float(pomer_list[1])
    pomer11_value = float(pomer11[0]) / float(pomer11[1])
    pomer22 = pomer2.split(":")
    if "/" in pomer22[0]:
        pomer_list = pomer22[0].split("/")
        pomer22[0] = float(pomer_list[0]) / float(pomer_list[1])
    if "/" in pomer22[1]:
        pomer_list = pomer22[1].split("/")
        pomer22[1] = float(pomer_list[0]) / float(pomer_list[1])
    pomer22_value = float(pomer22[0]) / float(pomer22[1])
    return [pomer11_value, pomer22_value]

def postupny_pomer(pomer, cislo, em):
    pomer_values = pomer.split(":")
    soucet = 0
    for i in pomer_values:
        soucet = float(soucet) + float(i)
    em.add_field(name="1.", value="Sečteme čísla poměru, výsledek: " + str(soucet))
    jeden_dil = round(float(cislo) / float(soucet), 2)
    em.add_field(name="2.", value="Vypočítáme jeden díl (číslo děleno součet), výsledek: " + str(jeden_dil))
    answers = []
    for i in pomer_values:
        answers.append(float(i) * jeden_dil)
    em.add_field(name="3.", value="Vynásobíme každé číslo poměru jedním dílem.")
    return answers

def gcd_func(value_list):
    buffer = 0
    if len(value_list) == 2:
        buffer = gcd(int(value_list[0]), int(value_list[1]))
    elif len(value_list) > 2:
        buffer = gcd(int(value_list[0]), int(value_list[1]))
        for i in value_list[2:]:
            buffer = gcd(int(i), buffer)
    else:
        raise ValueError
    return buffer


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    message_content = message.content.lower()


    if '?pomer ' in message_content:
        print("got query for [" + message_content + "]")
        content = message_content.replace("?pomer ", "").replace(",", ".")
        values = content.split(" ")
        pomer_list = values[0].split(":")
        try:
            answers = pomer(values[1], pomer_list[0], pomer_list[1])
            em = discord.Embed(title="Výsledky", description='requested by:\n{0}'.format(message.author))
            em.set_thumbnail(url=message.author.avatar_url)
            em.colour = randint(0, 0xffffff)
            em.add_field(name="Výstup 1", value=answers[0])
            em.add_field(name="Výstup 2", value=answers[1])
            await message.channel.send(embed=em)
            print("query processed")
        except (TypeError, IndexError) as e:
            await message.channel.send("what are you trying to do, idiot")
            print("error for query [" + message_content + "]")
            print(e)
    if '?umera ' in message_content:
        print("got query for [" + message_content + "]")
        content = message_content.replace("?umera ", "").replace(",", ".")
        values = content.split(" ")
        try:
            answers = umera(values[0], values[1])
            em = discord.Embed(title="Výsledky", description='requested by:\n{0}'.format(message.author))
            em.set_thumbnail(url=message.author.avatar_url)
            em.colour = randint(0, 0xffffff)
            em.add_field(name="Výstup 1", value=answers[0])
            em.add_field(name="Výstup 2", value=answers[1])
            await message.channel.send(embed=em)
            print("query processed")
        except (TypeError, IndexError) as e:
            await message.channel.send("what are you trying to do, idiot")
            print("error for query [" + message_content + "]")
            print(e)
    if '?help' in message_content:
        print("got query for [" + message_content + "]")
        em = discord.Embed(title="Commands")
        em.colour = randint(0, 0xffffff)
        em.add_field(name="Poměr", value="?pomer <pomer> <number>")
        em.add_field(name="Úměra", value="?umera <pomer> <pomer>")
        em.add_field(name="Zkracování zlomků", value="?rf <zlomek>")
        em.add_field(name="Největší společný dělitel", value="?gcd <cisla>")
        em.add_field(name="Postupný poměr", value="?pp <pomer> <cislo>")
        em.add_field(name="Zkracování postupného poměru", value="?pps <pomer>")
        em.set_footer(text='Developed by zlataovce, 2021')
        await message.channel.send(embed=em)
        print("query processed")
    if '?rf ' in message_content:
        print("got query for [" + message_content + "]")
        content = message_content.replace("?rf ", "").replace(",", ".")
        value_list = content.split("/")
        em = discord.Embed(title="Výsledky", description='requested by:\n{0}'.format(message.author))
        em.set_thumbnail(url=message.author.avatar_url)
        em.colour = randint(0, 0xffffff)
        try:
            em.add_field(name="Výstup 1", value=Fraction(int(value_list[0]), int(value_list[1])))
            await message.channel.send(embed=em)
            print("query processed")
        except (TypeError, IndexError, ValueError) as e:
            await message.channel.send("what are you trying to do, idiot")
            print("error for query [" + message_content + "]")
            print(e)
    if '?gcd ' in message_content:
        print("got query for [" + message_content + "]")
        content = message_content.replace("?gcd ", "").replace(",", ".")
        value_list = content.split(" ")
        em = discord.Embed(title="Výsledky", description='requested by:\n{0}'.format(message.author))
        em.set_thumbnail(url=message.author.avatar_url)
        em.colour = randint(0, 0xffffff)
        try:
            em.add_field(name="Výstup 1", value=gcd_func(value_list))
            await message.channel.send(embed=em)
            print("query processed")
        except (TypeError, IndexError, ValueError) as e:
            await message.channel.send("what are you trying to do, idiot")
            print("error for query [" + message_content + "]")
            print(e)
    if '?pp ' in message_content:
        print("got query for [" + message_content + "]")
        content = message_content.replace("?pp ", "").replace(",", ".")
        value_list = content.split(" ")
        em = discord.Embed(title="Výsledky", description='requested by:\n{0}'.format(message.author))
        em.set_thumbnail(url=message.author.avatar_url)
        em.colour = randint(0, 0xffffff)
        em2 = discord.Embed(title="Postup")
        em2.colour = randint(0, 0xffffff)
        try:
            answers = postupny_pomer(value_list[0], value_list[1], em2)
            count = 0
            for i in answers:
                count += 1
                em.add_field(name="Výstup " + str(count), value=round(i, 3))
            await message.channel.send(embed=em2)
            await message.channel.send(embed=em)
            print("query processed")
        except (TypeError, IndexError, ValueError) as e:
            await message.channel.send("what are you trying to do, idiot")
            print("error for query [" + message_content + "]")
            print(e)
    if '?pps ' in message_content:
        print("got query for [" + message_content + "]")
        content = message_content.replace("?pps ", "").replace(",", ".")
        value_list = content.split(" ")
        em = discord.Embed(title="Výsledky", description='requested by:\n{0}'.format(message.author))
        em.set_thumbnail(url=message.author.avatar_url)
        em.colour = randint(0, 0xffffff)
        try:
            pomer_values = value_list[0].split(":")
            gcd_number = gcd_func(pomer_values)
            final_value = ""
            count = 0
            for i in pomer_values:
                if count == 0:
                    count += 1
                    final_value = str(float(i) / float(gcd_number))
                else:
                    final_value = final_value + ":" + str(float(i) / float(gcd_number))
            em.add_field(name="Výstup 1", value=final_value)
            em.add_field(name="Poznámka", value="Poměr vynásoben číslem " + str(gcd_number) + ".")
            await message.channel.send(embed=em)
            print("query processed")
        except (TypeError, IndexError, ValueError) as e:
            await message.channel.send("what are you trying to do, idiot")
            print("error for query [" + message_content + "]")
            print(e)


client.run(TOKEN)
